import axios from "axios"

const instance = axios.create({
  baseURL: `https://api-sepomex.hckdrk.mx`,
});

export default instance