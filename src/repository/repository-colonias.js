import repository from "./repository"

export default {
  getColonies (cp) {
    return repository.get(`/query/info_cp/${cp}?type=simplified`)
  }
}