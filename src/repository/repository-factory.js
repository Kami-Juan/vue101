import RepositoryColonias from "./repository-colonias"

const repositories = {
  colonias: RepositoryColonias
}

export const RepositoryFactory = {
  get: name => repositories[name]
}